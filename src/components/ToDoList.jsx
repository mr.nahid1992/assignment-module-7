const ToDoList = ({ toDoListData,onDeleteTask,onCompleteTask }) => {
  return (
    <div className="todo-container">
      <ul className="todo-list">
        {
          toDoListData.map((task) => (
            <div className={`todo ${task.is_done ?'completed' :''} `} key={task.id}    >
              <li key={task.id} className="todo-item">
               {task.task} 
              
              </li>
              <button className="complete-btn complete" onClick={() =>onCompleteTask(task.id)}>
                  <span aria-hidden="true">Comple</span>
              </button>
              <button type="button" className="trash-btn close"  onClick={() => onDeleteTask(task.id)} aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
           
          ))
        }
      </ul>
    </div>
  );
};

export default ToDoList;
