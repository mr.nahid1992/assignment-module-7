import { useState } from "react";

const Form = ({ onAddTask }) => {
  const [task, setTask] = useState('');

  const handleInputChange = (event) => {
    setTask(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if(!task) return; 
    const newTask = {
      id: Date.now(),
      task: task,
      is_done: false
    };
    onAddTask(newTask);
    setTask('');
  }

  return (
    <div>
      <form className="add-task-form" onSubmit={handleSubmit}>
        <input type="text" value={task} onChange={handleInputChange} className=" todo-inputs" />
        <button type="submit" className="todo-button">+</button>
      </form>
    </div>
  );
};

export default Form;
