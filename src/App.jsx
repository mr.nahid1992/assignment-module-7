import './App.css'
import { useState } from "react";
import Form from './components/Form'
import ToDoList from './components/ToDoList'

function App() {
  const [toDoList, setToDoList] = useState([]);

  const makeToDoList = (newTask) => {
    setToDoList([...toDoList, newTask]);
  }

  const deleteTask=(id)=>{
    let uspdateList=toDoList.filter((task)=> task.id!==id);
    setToDoList((toDoList)=>uspdateList)

  }

  const completeTask=(id) =>{
    console.log(id,"complete");
    setToDoList((toDoList) =>
            toDoList.map((item) =>
                item.id === id ? { ...item, is_done: !item.is_done } : item
            )
    );
  }

  return (
    <>
      <header>
        <h1>To do List</h1>
      </header>
      <Form onAddTask={makeToDoList} />
      <ToDoList toDoListData={toDoList} onDeleteTask={deleteTask} onCompleteTask={completeTask}/>
    </>
  )
}

export default App;
